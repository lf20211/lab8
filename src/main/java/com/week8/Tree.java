package com.week8;

public class Tree {
    private String name;
    private int x;
    private int y;

    public Tree(String name, int i, int x, int y){
        this.name = name;
        this.x = x;
        this.y = y;
    }
    public void print(){
        System.out.println(name + " x:" + x + " y:" + y);
    }
}
